package com.mycompany.l03.cookies;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

class Loader {

    private static final String APP_PROPERTIES = "src/test/config//application.properties";
    private Properties properties;

    public Loader(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(APP_PROPERTIES));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("application.properties not found at " + APP_PROPERTIES);
        }
    }

    public String getLogin(){
        String driverPath = properties.getProperty("login");
        if(driverPath!= null) return driverPath;
        else throw new RuntimeException("login  not specified in the application.properties file.");
    }

    public String getPassword() {
        String url = properties.getProperty("password");
        if(url != null) return url;
        else throw new RuntimeException("password not specified in the application.properties file.");
    }
}
