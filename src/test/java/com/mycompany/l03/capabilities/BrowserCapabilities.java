package com.mycompany.l03.capabilities;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;

public class BrowserCapabilities {

    public static Capabilities getCapabilities(Browsers browser) {
        MutableCapabilities capabilities;
//        Proxy proxy = new Proxy()
//                .setProxyAutoconfigUrl("http://wpadvrn.t-systems.ru/wpad.dat");

        switch (browser) {
            case FIREFOX: {
                capabilities = new FirefoxOptions()
                        //firefox configs - http://kb.mozillazine.org/About:config_entries
                        //firefox preferences - http://kb.mozillazine.org/Category:Preferences
                        .addPreference("app.update.auto", false)
                        .addPreference("app.update.enabled", false)
                        .addPreference("network.proxy.no_proxies_on", "localhost");
                capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
                break;
            }

            case CHROME: {
                //https://peter.sh/experiments/chromium-command-line-switches/
                capabilities = new ChromeOptions()
                        .addArguments("--start-maximized")
                        .addArguments("--ignore-certificate-errors");
                break;
            }

            default:
                throw new IllegalArgumentException("Invalid browser: " + browser);
        }

        capabilities.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
        return capabilities;
    }
}
