package com.mycompany.l03.cookies;

import com.mycompany.l03.capabilities.BrowserCapabilities;
import com.mycompany.l03.capabilities.Browsers;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import ru.stqa.selenium.factory.WebDriverPool;

import java.util.concurrent.TimeUnit;

public class TestBase {

    WebDriver driver;

    @BeforeMethod
    public void initData() {
        driver = WebDriverPool.DEFAULT.getDriver(BrowserCapabilities.getCapabilities(Browsers.FIREFOX));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://intra.t-systems.ru");
    }

    @AfterMethod
    public void stop() {
        WebDriverPool.DEFAULT.dismissAll();
    }

}
