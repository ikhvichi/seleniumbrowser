package com.mycompany.l03.cookies;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CookiesTests extends TestBase {

    private static final Path COOKIES_PATH = Paths.get("src/test/resources/cookies.data");
    static Loader loader = new Loader();
    public static final String USERNAME = loader.getLogin();
    public static final String PASSWORD = loader.getPassword();


    @Test(priority = 1)
    public void saveCookies() throws IOException, InterruptedException {
        WebElement name = driver.findElement(By.id("LOGINUSERNAME"));
        name.clear();
        name.sendKeys(USERNAME);

        WebElement password = driver.findElement(By.id("LOGINPASSWORD"));
        password.clear();
        password.sendKeys(PASSWORD);

        WebElement logonBtn = driver.findElement(By.id("LOGINENTERBUTTON"));
        logonBtn.click();
        Thread.sleep(10000);
        //save cookies to a file
        Function<Cookie, String> cookieStr = cookie ->
                cookie.getName() + ";" +
                        cookie.getValue() + ";" +
                        cookie.getDomain() + ";" +
                        cookie.getPath() + ";" +
                        cookie.getExpiry() + ";" +
                        cookie.isSecure();

        if (!Files.exists(COOKIES_PATH)) {
            Files.createFile(COOKIES_PATH);
        }

        Files.write(
                COOKIES_PATH,
                driver.manage().getCookies()
                        .stream()
                        .map(cookieStr)
                        .collect(Collectors.joining("\n")).getBytes()
        );
    }

    @Test(priority = 2)
    public void runWithCookies() throws IOException, InterruptedException {
        Consumer<String> strCookie = str -> {
            var cookieLines = List.of(str.split("\n"));
            cookieLines.forEach(line -> {
                var cookieData = List.of(line.split(";"));

                var name = cookieData.get(0);
                var value = cookieData.get(1);
                var domain = cookieData.get(2);
                var path = cookieData.get(3);
                var isSecure = Boolean.parseBoolean(cookieData.get(5));

                Cookie cookie = new Cookie.Builder(name, value)
                        .domain(domain)
                        .path(path)
                        .isSecure(isSecure)
                        .build();
                driver.manage().addCookie(cookie);
            });
        };

        //read cookies from a file and open Intra
        Files.lines(COOKIES_PATH).forEach(strCookie);
        driver.get("https://intra.t-systems.ru/dash");
        Thread.sleep(10000);
    }
}
